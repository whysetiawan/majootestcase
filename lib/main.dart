import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:majootestcase/features/login/cubit/auth_cubit.dart';
import 'package:majootestcase/routes/app_routes.dart';
import 'package:majootestcase/shared/constants/constant.dart';
import 'package:majootestcase/shared/constants/app_colors.dart';
import 'package:flutter/foundation.dart';
import 'package:majootestcase/shared/constants/styles.dart';
import 'package:majootestcase/shared/helpers/app_bloc_observer.dart';
import 'package:majootestcase/shared/helpers/db_service.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final storage = await HydratedStorage.build(
    storageDirectory: await getTemporaryDirectory(),
  );
  HydratedBlocOverrides.runZoned(
    () => runApp(const MyApp()),
    storage: storage,
    blocObserver: AppBlocObserver(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => DbService(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthCubit>(
            create: (context) => AuthCubit(
              dbService: RepositoryProvider.of<DbService>(context),
            ),
          ),
        ],
        child: ScreenUtilInit(
          designSize: const Size(
            ScreenUtilConstants.width,
            ScreenUtilConstants.height,
          ),
          builder: () {
            return MaterialApp(
              title: 'Flutter Demo',
              builder: (context, widget) {
                //add this line
                ScreenUtil.setContext(context);
                return MediaQuery(
                  //Setting font does not change with system font size
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                  child: widget!,
                );
              },
              theme: ThemeData(
                primarySwatch: AppColors.primary,
                visualDensity: VisualDensity.adaptivePlatformDensity,
                fontFamily: Fonts.mulish,
              ).copyWith(
                inputDecorationTheme: appInputTheme,
                elevatedButtonTheme: appButtonTheme,
                backgroundColor: Colors.white,
                scaffoldBackgroundColor: Colors.white,
                appBarTheme: AppBarTheme.of(context).copyWith(
                  centerTitle: true,
                  titleTextStyle: const TextStyle(
                    fontSize: 18,
                    fontFamily: Fonts.mulish,
                    fontWeight: FontWeight.w700,
                    color: Colors.white,
                  ),
                  iconTheme: const IconThemeData(color: Colors.white),
                  elevation: 0.0,
                  systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
                    statusBarColor: Colors.transparent,
                    // statusBarIconBrightness: B
                  ),
                ),
              ),
              onGenerateRoute: AppRoutes().onGenerateRoute,
            );
          },
        ),
      ),
    );
  }
}
