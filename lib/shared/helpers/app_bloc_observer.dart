import 'dart:developer';

import 'package:flutter_bloc/flutter_bloc.dart';

class AppBlocObserver implements BlocObserver {
  @override
  void onChange(BlocBase bloc, Change change) {
    log("╔╣ BLOC CHANGE: ${bloc.toString()}");
    log("╠╣ CURRENT STATE: ${change.currentState}");
    log("╚╣ NEXT STATE: ${change.nextState}");
  }

  @override
  void onClose(BlocBase bloc) {
    log("╔╣ BLOC CLOSE: ${bloc.toString()}");
    log("╚╣ BLOC STATE: ${bloc.state}");
  }

  @override
  void onCreate(BlocBase bloc) {
    log("╔╣ BLOC CREATE: ${bloc.toString()}");
    log("╚╣ BLOC STATE: ${bloc.state}");
  }

  @override
  void onError(BlocBase bloc, Object error, StackTrace stackTrace) {
    log("╔╣ BLOC ERROR: ${bloc.toString()}");
    log("╠╣ ERROR TYPE: ${error.toString()}");
    log("╚╣ STACK TRACE: ${stackTrace.toString()}");
  }

  @override
  void onEvent(Bloc bloc, Object? event) {
    log("╔╣ BLOC EVENT: ${bloc.toString()}");
    log("╠╣ EVENT TYPE: ${event.toString()}");
    log("╚╣ BLOC STATE: ${bloc.state}");
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    log("╔╣ BLOC TRANSITION: ${bloc.toString()}");
    log("╠╣ CURRENT TRANSITION STATE: ${transition.currentState}");
    log("╠╣ NEXT TRANSITION STATE: ${transition.nextState}");
  }
}
