import 'dart:async';
import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:flutter/foundation.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

Dio? dioInstance;
createInstance() async {
  var options = BaseOptions(
      baseUrl: "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr",
      connectTimeout: 12000,
      receiveTimeout: 12000,
      headers: {
        "x-rapidapi-key": "6af4f77398mshb3f6cd027729468p1d6d31jsn038947888a17",
        "x-rapidapi-host": "imdb8.p.rapidapi.com"
      });
  dioInstance = Dio(options);
  dioInstance!.interceptors.add(PrettyDioLogger(
      requestHeader: true,
      requestBody: true,
      responseBody: true,
      responseHeader: false,
      error: true,
      compact: true,
      maxWidth: 90));
}

Future<Dio?> dio() async {
  await createInstance();
  dioInstance!.options.baseUrl =
      "https://imdb8.p.rapidapi.com/auto-complete?q=game%20of%20thr";
  return dioInstance;
}

class DioClient {
  static final DioClient _singleton = DioClient._internal();
  factory DioClient() => _singleton;

  DioClient._internal();

  final BaseOptions options = BaseOptions(
    baseUrl: "https://imdb8.p.rapidapi.com/",
    connectTimeout: 30000,
    receiveTimeout: 60000,
    headers: {
      "x-rapidapi-key": "92a98e2e31msh03137dc00f6e012p15461cjsn83b85100d853",
      "x-rapidapi-host": "imdb8.p.rapidapi.com",
    },
  );

  final Dio _dio = Dio();

  Dio get request {
    _dio.options = options;
    if (!kReleaseMode) {
      _dio.interceptors.add(
        PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
        ),
      );
    }
    _dio.interceptors.add(
      DioCacheManager(
        CacheConfig(baseUrl: options.baseUrl),
      ).interceptor,
    );
    return _dio;
  }
}
