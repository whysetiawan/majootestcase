import 'package:majootestcase/features/login/models/user.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DbService {
  static final DbService _singleton = DbService._internal();
  factory DbService() => _singleton;

  DbService._internal();

  final String _dbName = 'test.db';
  final String _tableName = 'user';
  final String _columnId = 'id';
  final String _columnEmail = 'email';
  final String _columnPassword = 'password';

  Database? _db;

  Future<Database> get db async {
    return _db ?? await _initDb();
  }

  Future<Database> _initDb() async {
    final databasesPath = await getDatabasesPath();
    final path = join(databasesPath, _dbName);

    return await openDatabase(path, version: 1, onCreate: (
      Database db,
      int version,
    ) async {
      await db.execute('''
          CREATE TABLE IF NOT EXISTS $_tableName (
            $_columnId INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            $_columnEmail TEXT UNIQUE NOT NULL,
            $_columnPassword TEXT NOT NULL
          )
        ''');
    });
  }

  Future<User?> findUser(String email, String password) async {
    final db = await this.db;
    final List<Map<String, dynamic>> maps = await db.query(
      _tableName,
      columns: [_columnEmail, _columnPassword],
      where: '$_columnEmail = ? AND $_columnPassword = ?',
      whereArgs: [email, password],
    );

    if (maps.isNotEmpty) {
      return User.fromJson(maps.first);
    }

    return null;
  }

  Future<int> insert({
    required String email,
    required String password,
  }) async {
    final db = await this.db;
    final result = await db.insert(
      _tableName,
      {
        _columnEmail: email,
        _columnPassword: password,
      },
      conflictAlgorithm: ConflictAlgorithm.abort,
    );
    return result;
  }
}
