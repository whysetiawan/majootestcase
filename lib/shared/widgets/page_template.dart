import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class PageTemplate extends StatelessWidget {
  const PageTemplate({
    Key? key,
    required this.child,
    this.scrollable = true,
    this.appBar,
    this.overlayStyle,
    this.title,
    this.withoutAppBar = false,
    this.extendBodyBehindAppBar = false,
    this.useSafeArea = true,
  }) : super(key: key);

  final Widget child;
  final bool scrollable;

  final AppBar? appBar;

  final SystemUiOverlayStyle? overlayStyle;

  final String? title;
  final bool withoutAppBar;
  final bool extendBodyBehindAppBar;
  final bool useSafeArea;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: FocusScope.of(context).unfocus,
      child: Scaffold(
        extendBodyBehindAppBar: extendBodyBehindAppBar,
        appBar: withoutAppBar
            ? null
            : appBar ??
                AppBar(
                  title: Text(title ?? ''),
                  systemOverlayStyle: overlayStyle,
                ),
        body: scrollable
            ? _ScrollableTemplate(
                child: child,
                useSafeArea: useSafeArea,
              )
            : _DefaultTemplate(
                child: child,
                useSafeArea: useSafeArea,
              ),
      ),
    );
  }
}

class _DefaultTemplate extends StatelessWidget {
  const _DefaultTemplate({
    Key? key,
    required this.child,
    required this.useSafeArea,
  }) : super(key: key);

  final Widget child;
  final bool useSafeArea;

  @override
  Widget build(BuildContext context) {
    if (!useSafeArea) {
      return child;
    }
    return SafeArea(
      child: child,
    );
  }
}

class _ScrollableTemplate extends StatelessWidget {
  const _ScrollableTemplate({
    Key? key,
    required this.child,
    required this.useSafeArea,
  }) : super(key: key);

  final Widget child;
  final bool useSafeArea;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: _DefaultTemplate(
        child: child,
        useSafeArea: useSafeArea,
      ),
    );
  }
}
