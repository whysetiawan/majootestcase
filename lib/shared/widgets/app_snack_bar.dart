import 'package:flutter/material.dart';

class AppSnackBar {
  static successSnackBar({
    required String message,
  }) {
    return SnackBar(
      content: Text(message),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Colors.green[700],
    );
  }

  static errorSnackBar({
    required String message,
  }) {
    return SnackBar(
      content: Text(message),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Colors.red[700],
    );
  }
}
