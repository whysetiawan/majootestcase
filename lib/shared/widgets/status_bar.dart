import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class StatusBar extends StatelessWidget {
  const StatusBar({
    Key? key,
    required this.child,
    this.overlayStyle,
    this.statusBarColor,
    this.statusBarIconBrightness,
  }) : super(key: key);

  final Widget child;
  final SystemUiOverlayStyle? overlayStyle;
  final Color? statusBarColor;
  final Brightness? statusBarIconBrightness;

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      child: child,
      value: overlayStyle ??
          SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: statusBarColor,
            statusBarIconBrightness: statusBarIconBrightness,
          ),
    );
  }
}
