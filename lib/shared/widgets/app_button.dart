import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class AppButton extends StatelessWidget {
  const AppButton({
    Key? key,
    required this.title,
    required this.onPressed,
    this.width,
    this.backgroundColor,
  }) : super(key: key);

  final double? width;
  final Color? backgroundColor;
  final String title;
  final VoidCallback? onPressed;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width ?? double.infinity,
      height: 42.w,
      child: ElevatedButton(
        child: Text(
          title,
          style: Theme.of(context).textTheme.headline6?.copyWith(
                color: Colors.white,
              ),
        ),
        onPressed: onPressed,
      ),
    );
  }
}
