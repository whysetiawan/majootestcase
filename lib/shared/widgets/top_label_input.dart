import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/shared/widgets/space.dart';

class TopLabelInput extends StatelessWidget {
  const TopLabelInput({
    Key? key,
    this.labelText,
    this.focusNode,
    this.onChanged,
    this.controller,
    this.decoration,
    this.enabled,
    this.keyboardType,
    this.style,
    this.inputFormatters,
    this.obscureText = false,
    this.onSaved,
    this.autoValidateMode = AutovalidateMode.onUserInteraction,
    this.validator,
  }) : super(key: key);

  final String? labelText;
  final FocusNode? focusNode;
  final ValueChanged<String>? onChanged;
  final TextEditingController? controller;
  final InputDecoration? decoration;
  final bool? enabled;
  final TextInputType? keyboardType;
  final TextStyle? style;
  final List<TextInputFormatter>? inputFormatters;
  final bool obscureText;
  final Function(String? value)? onSaved;
  final AutovalidateMode autoValidateMode;
  final FormFieldValidator<String>? validator;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          labelText ?? decoration?.labelText ?? '',
          style: const TextStyle(fontWeight: FontWeight.w600),
        ),
        VerticalSpace(height: 8.w),
        TextFormField(
          autovalidateMode: autoValidateMode,
          enabled: enabled,
          controller: controller,
          focusNode: focusNode,
          key: key,
          onChanged: onChanged,
          style: style,
          decoration: decoration,
          keyboardType: keyboardType,
          inputFormatters: inputFormatters,
          validator: validator,
          obscureText: obscureText,
          onSaved: onSaved,
        ),
      ],
    );
  }
}
