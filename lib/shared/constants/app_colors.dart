import 'dart:ui';

import 'package:flutter/material.dart';

class AppColors {
  static const int _primaryValue = 0xFF77B5BA;
  static const MaterialColor primary = MaterialColor(
    _primaryValue,
    <int, Color>{
      50: Color(_primaryValue),
      100: Color(_primaryValue),
      200: Color(_primaryValue),
      300: Color(_primaryValue),
      400: Color(_primaryValue),
      500: Color(_primaryValue),
      600: Color(_primaryValue),
      700: Color(_primaryValue),
      800: Color(_primaryValue),
      900: Color(_primaryValue),
    },
  );
  static const int _secondaryValue = 0xFF84C44C;
  static const MaterialColor secondary = MaterialColor(
    _secondaryValue,
    <int, Color>{},
  );
}
