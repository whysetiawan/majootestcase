class Fonts {
  static const mulish = 'Mulish';
}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}
