enum ProgressStatus {
  initial,
  loading,
  success,
  failure,
}

extension ProgressStatusX on ProgressStatus {
  bool get isLoading => this == ProgressStatus.loading;
  bool get isInitial => this == ProgressStatus.initial;
  bool get isSuccess => this == ProgressStatus.success;
  bool get isFailure => this == ProgressStatus.failure;
}
