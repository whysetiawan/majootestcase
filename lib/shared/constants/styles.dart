import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter/material.dart';
import 'package:majootestcase/shared/constants/app_colors.dart';

class Insets {
  static double get xs => 4.w;
  static double get sm => 8.w;
  static double get med => 12.w;
  static double get lg => 16.w;
  static double get xl => 20.w;
  static double get xxl => 32.w;
}

final appInputTheme = InputDecorationTheme(
  isDense: true,
  alignLabelWithHint: true,
  floatingLabelBehavior: FloatingLabelBehavior.never,
  filled: true,
  fillColor: Colors.transparent,
  labelStyle: TextStyle(color: Colors.grey[600]),
  focusColor: AppColors.primary,
  disabledBorder: const OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(color: Color(0xFFE3E6ED)),
  ),
  focusedErrorBorder: OutlineInputBorder(
    borderRadius: const BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(color: Colors.red[700]!, width: 1.0),
  ),
  errorBorder: OutlineInputBorder(
    borderRadius: const BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(
      color: Colors.red[700]!,
      width: 1.0,
    ),
  ),
  focusedBorder: const OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(
      color: AppColors.primary,
      width: 1.0,
    ),
  ),
  enabledBorder: OutlineInputBorder(
    borderRadius: const BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(
      color: Colors.grey[300]!,
      width: 1.0,
    ),
  ),
  border: OutlineInputBorder(
    borderRadius: const BorderRadius.all(Radius.circular(10)),
    borderSide: BorderSide(
      color: Colors.grey[300]!,
      width: 1.0,
    ),
  ),
);

final appButtonTheme = ElevatedButtonThemeData(
  style: ButtonStyle(
    minimumSize: MaterialStateProperty.all<Size>(
      const Size(
        0,
        0,
      ),
    ),
    backgroundColor:
        MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states) {
      if (states.contains(MaterialState.disabled)) {
        return Colors.grey[300]!;
      }
      return AppColors.secondary;
    }),
    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
    ),
    elevation: MaterialStateProperty.all<double>(0.0),
  ),
);
