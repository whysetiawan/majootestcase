import 'package:flutter/material.dart';
import 'package:majootestcase/features/error/connection_error_page.dart';
import 'package:majootestcase/features/home/movie/movie_detail_page.dart';
import 'package:majootestcase/features/home/movie/movie_list_page.dart';

class HomeRoutes {
  Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case movieListPageRoute:
        return MaterialPageRoute(builder: (_) => const MovieListPage());
      case movieDetailPageRoute:
        return MaterialPageRoute(builder: (_) => const MovieDetailPage());
      case connectionErrorPageRoute:
        return MaterialPageRoute(
          builder: (_) => const ConnectionErrorPage(),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => const ConnectionErrorPage(),
        );
      // return MaterialPageRoute(builder: (_) => const MovieListPage());
    }
  }

  pageRoute(dynamic page) {
    return MaterialPageRoute(builder: (_) => page);
  }
}
