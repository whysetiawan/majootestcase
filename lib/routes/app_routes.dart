import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/features/home/home_navigator.dart';
import 'package:majootestcase/features/login/login_page.dart';
import 'package:majootestcase/features/register/cubit/register_cubit.dart';
import 'package:majootestcase/features/register/register_page.dart';
import 'package:majootestcase/features/splash_screen/splash_screen_page.dart';
import 'package:majootestcase/shared/helpers/db_service.dart';

class AppRoutes {
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case splashScreenPageRoute:
        return pageRoute(const SplashScreenPage());
      case loginPageRoute:
        return pageRoute(LoginPage());
      case registerPageRoute:
        return pageRoute(
          BlocProvider(
            create: (context) => RegisterCubit(
              dbService: RepositoryProvider.of<DbService>(context),
            ),
            child: const RegisterPage(),
          ),
        );
      case homePageRoute:
        return pageRoute(HomeNavigator());
      default:
        return pageRoute(LoginPage());
    }
  }

  pageRoute(dynamic page) {
    return MaterialPageRoute(builder: (_) => page);
  }
}
