part of 'auth_cubit.dart';

class AuthState extends Equatable {
  final ProgressStatus loginProgressStatus;
  final AuthStatus authStatus;
  final bool isPasswordVisible;
  final User user;
  final String errMsg;

  const AuthState({
    this.loginProgressStatus = ProgressStatus.initial,
    this.authStatus = AuthStatus.unauthenticated,
    this.isPasswordVisible = false,
    this.user = const User(),
    this.errMsg = "",
  });

  @override
  List<Object> get props => [
        loginProgressStatus,
        authStatus,
        isPasswordVisible,
        user,
        errMsg,
      ];

  AuthState copyWith({
    ProgressStatus? loginProgressStatus,
    AuthStatus? authStatus,
    bool? isPasswordVisible,
    User? user,
    String? errMsg,
  }) {
    return AuthState(
      loginProgressStatus: loginProgressStatus ?? this.loginProgressStatus,
      authStatus: authStatus ?? this.authStatus,
      isPasswordVisible: isPasswordVisible ?? this.isPasswordVisible,
      user: user ?? this.user,
      errMsg: errMsg ?? this.errMsg,
    );
  }

  @override
  String toString() {
    return 'AuthState(loginProgressStatus: $loginProgressStatus, authStatus: $authStatus, isPasswordVisible: $isPasswordVisible, user: $user, errMsg: $errMsg)';
  }
}
