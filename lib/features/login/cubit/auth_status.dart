part of 'auth_cubit.dart';

enum AuthStatus {
  unauthenticated,
  authenticated,
}

extension AuthStatusX on AuthStatus {
  bool get isAuthenticated => this == AuthStatus.authenticated;
  bool get isUnauthenticated => this == AuthStatus.unauthenticated;
}
