import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:majootestcase/features/login/models/user.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';
import 'package:majootestcase/shared/helpers/db_service.dart';

part 'auth_state.dart';
part 'auth_status.dart';

class AuthCubit extends HydratedCubit<AuthState> {
  AuthCubit({
    required this.dbService,
  }) : super(const AuthState());

  final DbService dbService;

  void authStatusChanged(AuthStatus authStatus) {
    emit(
      state.copyWith(
        authStatus: authStatus,
      ),
    );
  }

  Future<void> login(String email, String password) async {
    try {
      emit(state.copyWith(loginProgressStatus: ProgressStatus.loading));
      final res = await dbService.findUser(email, password);
      if (res != null) {
        emit(
          state.copyWith(
            loginProgressStatus: ProgressStatus.success,
            user: res,
          ),
        );
        authStatusChanged(AuthStatus.authenticated);
      } else {
        authStatusChanged(AuthStatus.unauthenticated);
        throw "Invalid Username or Password";
      }
    } catch (e) {
      emit(
        state.copyWith(
          loginProgressStatus: ProgressStatus.failure,
          errMsg: e.toString(),
        ),
      );
    }
  }

  void toggleVisiblity() {
    emit(
      state.copyWith(
        isPasswordVisible: !state.isPasswordVisible,
      ),
    );
  }

  @override
  AuthState fromJson(Map<String, dynamic> json) {
    return AuthState(
      authStatus: json['isLoggedIn'] == true
          ? AuthStatus.authenticated
          : AuthStatus.unauthenticated,
      user: json['user'] != null ? User.fromJson(json['user']) : const User(),
    );
  }

  @override
  Map<String, dynamic> toJson(AuthState state) {
    return {
      'isLoggedIn': state.authStatus.isAuthenticated,
      'user': state.user.toJson(),
    };
  }
}
