import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/features/home/home_navigator.dart';
import 'package:majootestcase/features/login/cubit/auth_cubit.dart';
import 'package:majootestcase/features/register/register_page.dart';
import 'package:majootestcase/shared/constants/app_icons.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';
import 'package:majootestcase/shared/constants/styles.dart';
import 'package:majootestcase/shared/helpers/form_validator.dart';
import 'package:majootestcase/shared/widgets/app_button.dart';
import 'package:majootestcase/shared/widgets/app_snack_bar.dart';
import 'package:majootestcase/shared/widgets/page_template.dart';
import 'package:majootestcase/shared/widgets/space.dart';
import 'package:majootestcase/shared/widgets/top_label_input.dart';

const loginPageRoute = '/login';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);
  final _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController();

  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      overlayStyle: SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.transparent,
      ),
      withoutAppBar: true,
      scrollable: true,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Insets.xl,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Image.asset(
                AppIcons.icLogo,
                width: 180.w,
                height: 180.w,
              ),
            ),
            VerticalSpace(height: 16.w),
            Text(
              "Welcome !",
              style: Theme.of(context).textTheme.headline5,
            ),
            Text(
              "Please login to continue",
              style: Theme.of(context).textTheme.bodyText2,
            ),
            VerticalSpace(height: 8.w),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TopLabelInput(
                    autoValidateMode: AutovalidateMode.onUserInteraction,
                    controller: _emailController,
                    labelText: "Email",
                    validator: (value) {
                      if (value?.isEmpty == true) {
                        return "Email is required";
                      }
                      if (FormValidator.isEmail(value ?? '')) {
                        return "Email is invalid";
                      }
                      return null;
                    },
                  ),
                  VerticalSpace(height: 16.w),
                  BlocBuilder<AuthCubit, AuthState>(
                    builder: (context, state) {
                      return TopLabelInput(
                        autoValidateMode: AutovalidateMode.onUserInteraction,
                        controller: _passwordController,
                        labelText: "Password",
                        obscureText: !state.isPasswordVisible,
                        decoration: InputDecoration(
                          suffixIcon: InkWell(
                            onTap: BlocProvider.of<AuthCubit>(context)
                                .toggleVisiblity,
                            child: Icon(
                              !state.isPasswordVisible
                                  ? Icons.visibility_off_rounded
                                  : Icons.visibility_rounded,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            VerticalSpace(height: 20.w),
            BlocConsumer<AuthCubit, AuthState>(
              listener: (context, state) {
                if (state.loginProgressStatus.isFailure) {
                  ScaffoldMessenger.of(context)
                    ..hideCurrentSnackBar()
                    ..showSnackBar(
                      AppSnackBar.errorSnackBar(
                        message: state.errMsg,
                      ),
                    );
                }
                if (state.loginProgressStatus.isSuccess) {
                  ScaffoldMessenger.of(context)
                    ..hideCurrentSnackBar()
                    ..showSnackBar(
                      AppSnackBar.successSnackBar(
                        message: "Login Successful",
                      ),
                    );
                  Navigator.of(context).pushReplacementNamed(homePageRoute);
                }
              },
              builder: (context, state) {
                return AppButton(
                  title: "Login",
                  onPressed: () {
                    BlocProvider.of<AuthCubit>(context).login(
                      _emailController.text,
                      _passwordController.text,
                    );
                  },
                );
              },
            ),
            VerticalSpace(height: 12.w),
            TextButton(
              onPressed: () {
                Navigator.pushNamed(context, registerPageRoute);
              },
              child: const Text(
                "Belum punya akun ?",
              ),
            ),
          ],
        ),
      ),
    );
  }
}
