part of 'connectivity_cubit.dart';

class ConnectivityState extends Equatable {
  final bool isConnected;
  final ProgressStatus progressStatus;

  const ConnectivityState({
    this.isConnected = true,
    this.progressStatus = ProgressStatus.initial,
  });

  @override
  List<Object> get props => [isConnected, progressStatus];

  ConnectivityState copyWith({
    bool? isConnected,
    ProgressStatus? progressStatus,
  }) {
    return ConnectivityState(
      isConnected: isConnected ?? this.isConnected,
      progressStatus: progressStatus ?? this.progressStatus,
    );
  }

  @override
  String toString() =>
      'ConnectivityState(isConnected: $isConnected, progressStatus: $progressStatus)';
}
