import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';

part 'connectivity_state.dart';

class ConnectivityCubit extends Cubit<ConnectivityState> {
  StreamSubscription? _subscription;
  ConnectivityCubit() : super(const ConnectivityState()) {
    _subscription =
        Connectivity().onConnectivityChanged.listen(changeIsConnected);
  }

  void changeIsConnected(ConnectivityResult event) {
    if (event == ConnectivityResult.mobile ||
        event == ConnectivityResult.wifi) {
      emit(
        state.copyWith(isConnected: true),
      );
    } else {
      emit(
        state.copyWith(isConnected: false),
      );
    }
  }

  void connectivityCheck() async {
    emit(state.copyWith(progressStatus: ProgressStatus.loading));
    final ConnectivityResult _result = await Connectivity().checkConnectivity();
    Future.delayed(const Duration(milliseconds: 1500), () {
      emit(state.copyWith(progressStatus: ProgressStatus.success));
    });
    changeIsConnected(_result);
  }

  @override
  Future<void> close() {
    _subscription!.cancel();
    return super.close();
  }
}
