import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/features/error/connection_error_page.dart';
import 'package:majootestcase/features/home/cubit/connectivity_cubit.dart';
import 'package:majootestcase/features/home/movie/bloc/movie_bloc.dart';
import 'package:majootestcase/features/home/movie/movie_list_page.dart';
import 'package:majootestcase/routes/home_routes.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';

const homePageRoute = '/home';

class HomeNavigator extends StatelessWidget {
  final _navigatorKey = GlobalKey<NavigatorState>();

  HomeNavigator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        final canPop = Navigator.canPop(
          _navigatorKey.currentState?.context ?? context,
        );
        if (canPop) {
          _navigatorKey.currentState?.maybePop();
          return Future.value(false);
        }
        return Future.value(true);
      },
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (_) => ConnectivityCubit()..connectivityCheck(),
          ),
          BlocProvider(
            create: (_) => MovieBloc(),
          ),
        ],
        child: BlocListener<ConnectivityCubit, ConnectivityState>(
          listenWhen: (previous, current) =>
              previous.isConnected != current.isConnected ||
              current.progressStatus.isInitial,
          listener: (context, state) {
            if (!state.isConnected) {
              // ScaffoldMessenger.of(context).showSnackBar(
              //   AppSnackBar.errorSnackBar(message: "No internet connection"),
              // );
              Navigator.pushNamed(
                _navigatorKey.currentState?.context ?? context,
                connectionErrorPageRoute,
              );
            } else if (state.isConnected) {
              Navigator.pop(
                _navigatorKey.currentState?.context ?? context,
              );
            }
          },
          child: Navigator(
            key: _navigatorKey,
            initialRoute: movieListPageRoute,
            onGenerateInitialRoutes: (navigator, initialRoute) {
              return [
                HomeRoutes().generateRoute(
                  RouteSettings(
                    name: initialRoute,
                  ),
                ),
              ];
            },
            onGenerateRoute: HomeRoutes().generateRoute,
          ),
        ),
      ),
    );
  }
}
