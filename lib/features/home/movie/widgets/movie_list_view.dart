import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/features/home/movie/bloc/movie_bloc.dart';
import 'package:majootestcase/features/home/movie/models/movie_response_model.dart';
import 'package:majootestcase/features/home/movie/movie_detail_page.dart';
import 'package:majootestcase/shared/constants/styles.dart';

class MovieListView extends StatelessWidget {
  const MovieListView({
    Key? key,
    required this.movies,
  }) : super(key: key);

  final MovieResponseModel movies;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.separated(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(horizontal: Insets.med),
        itemCount: movies.d?.length ?? 0,
        itemBuilder: (context, index) {
          final item = movies.d![index];
          return Card(
            child: ListTile(
              onTap: () {
                BlocProvider.of<MovieBloc>(context).add(
                  MovieEventSelectMovie(item),
                );
                Navigator.pushNamed(
                  context,
                  movieDetailPageRoute,
                );
              },
              leading: Container(
                width: 60.w,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.w),
                  image: DecorationImage(
                    image: CachedNetworkImageProvider(
                      item.i?.imageUrl ??
                          "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png",
                    ),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              title: Text("${item.l} (${item.y})"),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(item.q ?? ""),
                  RichText(
                    text: TextSpan(
                      children: [
                        const TextSpan(text: "Stars : "),
                        TextSpan(
                          text: item.s ?? "",
                          style: const TextStyle(
                            color: Colors.blue,
                          ),
                        ),
                      ],
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ),
                ],
              ),
              isThreeLine: true,
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) {
          return const SizedBox();
        },
      ),
    );
  }
}
