import 'package:flutter/material.dart';

class MovieDescText extends StatelessWidget {
  const MovieDescText({
    Key? key,
    required this.title,
    required this.text,
  }) : super(key: key);

  final String title;
  final String text;

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(text: title),
          TextSpan(
            text: text,
            style: const TextStyle(
              color: Colors.blue,
            ),
          ),
        ],
        style: Theme.of(context).textTheme.subtitle1,
      ),
    );
  }
}
