import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:majootestcase/features/home/movie/models/movie_response_model.dart';
import 'package:majootestcase/shared/helpers/dio_client.dart';

class MovieRepository {
  final DioClient dioClient = DioClient();

  Future<MovieResponseModel> getMovieList({
    required String keyword,
  }) async {
    final req = await dioClient.request.get(
      "/auto-complete",
      options: buildCacheOptions(
        const Duration(
          hours: 2,
        ),
      ),
      queryParameters: {
        "q": keyword,
      },
    );
    return MovieResponseModel.fromJson(req.data);
  }
}
