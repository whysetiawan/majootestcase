import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:majootestcase/features/home/movie/models/movie_response_model.dart';
import 'package:majootestcase/features/home/movie/repository/movie_repository.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';
import 'package:rxdart/rxdart.dart';

part 'movie_event.dart';
part 'movie_state.dart';

class MovieBloc extends HydratedBloc<MovieEvent, MovieState> {
  final MovieRepository _movieRepository = MovieRepository();

  MovieBloc() : super(const MovieState()) {
    on<MovieEventSearchMovieList>(
      getMovieList,
      transformer: (events, mapper) {
        return events
            .debounceTime(const Duration(seconds: 1))
            .switchMap(mapper);
      },
    );
    on<MovieEventSelectMovie>(selectMovie);
  }

  void selectMovie(
    MovieEventSelectMovie event,
    Emitter<MovieState> emit,
  ) {
    emit(
      state.copyWith(
        selectedMovie: event.selectedMovie,
      ),
    );
  }

  void getMovieList(
    MovieEventSearchMovieList event,
    Emitter<MovieState> emit,
  ) async {
    try {
      emit(state.copyWith(progressStatus: ProgressStatus.loading));
      final MovieResponseModel res = await _movieRepository.getMovieList(
        keyword: event.keyword,
      );
      emit(
        state.copyWith(
          progressStatus: ProgressStatus.success,
          movieResponseModel: res,
          keyword: event.keyword,
        ),
      );
    } on DioError catch (e) {
      if (e.type == DioErrorType.connectTimeout ||
          e.type == DioErrorType.receiveTimeout) {
        emit(
          state.copyWith(
            progressStatus: ProgressStatus.failure,
            errMsg: 'Request Timed out',
          ),
        );
      }
      if (e.type == DioErrorType.other) {
        if (e.error is SocketException) {
          emit(
            state.copyWith(
              progressStatus: ProgressStatus.failure,
              errMsg: 'No Internet Connection',
            ),
          );
        } else {
          emit(
            state.copyWith(
              progressStatus: ProgressStatus.failure,
              errMsg: 'Something went wrong, please try again later',
            ),
          );
        }
      }
    } catch (e) {
      emit(
        state.copyWith(
          progressStatus: ProgressStatus.failure,
          errMsg: e.toString(),
        ),
      );
    }
  }

  @override
  MovieState? fromJson(Map<String, dynamic> json) {
    return MovieState(
      keyword: json["keyword"],
      movieResponseModel: json["movieResponseModel"] != null
          ? MovieResponseModel.fromJson(
              json["movieResponseModel"],
            )
          : const MovieResponseModel(),
    );
  }

  @override
  Map<String, dynamic>? toJson(MovieState state) {
    return {
      "keyword": state.keyword,
      "movieResponseModel": state.movieResponseModel.toJson(),
    };
  }
}
