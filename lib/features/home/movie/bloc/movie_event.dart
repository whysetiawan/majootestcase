part of 'movie_bloc.dart';

abstract class MovieEvent extends Equatable {
  const MovieEvent();

  @override
  List<Object> get props => [];
}

class MovieEventSearchMovieList extends MovieEvent {
  final String keyword;

  const MovieEventSearchMovieList({
    required this.keyword,
  });

  @override
  List<Object> get props => [keyword];
}

class MovieEventSelectMovie extends MovieEvent {
  final D selectedMovie;

  const MovieEventSelectMovie(this.selectedMovie);

  @override
  List<Object> get props => [selectedMovie];
}
