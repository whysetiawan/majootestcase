part of 'movie_bloc.dart';

class MovieState extends Equatable {
  final ProgressStatus progressStatus;
  final String keyword;
  final MovieResponseModel movieResponseModel;
  final D selectedMovie;
  final String errMsg;

  const MovieState({
    this.progressStatus = ProgressStatus.initial,
    this.keyword = "",
    this.movieResponseModel = const MovieResponseModel(),
    this.errMsg = "",
    this.selectedMovie = const D(),
  });

  @override
  List<Object> get props {
    return [
      progressStatus,
      keyword,
      movieResponseModel,
      selectedMovie,
      errMsg,
    ];
  }

  MovieState copyWith({
    ProgressStatus? progressStatus,
    String? keyword,
    MovieResponseModel? movieResponseModel,
    D? selectedMovie,
    String? errMsg,
  }) {
    return MovieState(
      progressStatus: progressStatus ?? this.progressStatus,
      keyword: keyword ?? this.keyword,
      movieResponseModel: movieResponseModel ?? this.movieResponseModel,
      selectedMovie: selectedMovie ?? this.selectedMovie,
      errMsg: errMsg ?? this.errMsg,
    );
  }

  @override
  String toString() {
    return 'MovieState(progressStatus: $progressStatus, keyword: $keyword, movieResponseModel: $movieResponseModel, selectedMovie: $selectedMovie, errMsg: $errMsg)';
  }
}
