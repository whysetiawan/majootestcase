import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/features/home/movie/bloc/movie_bloc.dart';
import 'package:majootestcase/features/home/movie/widgets/movie_list_view.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';
import 'package:majootestcase/shared/constants/styles.dart';
import 'package:majootestcase/shared/widgets/page_template.dart';
import 'package:majootestcase/shared/widgets/space.dart';

const movieListPageRoute = '/movie-list';

class MovieListPage extends StatefulWidget {
  const MovieListPage({Key? key}) : super(key: key);

  @override
  State<MovieListPage> createState() => _MovieListPageState();
}

class _MovieListPageState extends State<MovieListPage> {
  final _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _searchController.text = BlocProvider.of<MovieBloc>(context).state.keyword;
  }

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      scrollable: false,
      title: "Movie List",
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          VerticalSpace(height: 16.w),
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: Insets.xl,
            ),
            child: TextFormField(
              controller: _searchController,
              decoration: const InputDecoration(
                prefixIcon: Icon(Icons.search, size: 30),
                hintText: "Search . . . ",
              ),
              onChanged: (value) {
                BlocProvider.of<MovieBloc>(context).add(
                  MovieEventSearchMovieList(keyword: value),
                );
              },
            ),
          ),
          BlocBuilder<MovieBloc, MovieState>(
            buildWhen: (previous, current) =>
                previous.progressStatus != current.progressStatus,
            builder: (context, state) {
              final movieList = state.movieResponseModel;
              if (state.progressStatus.isLoading) {
                return const Expanded(
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
              if (movieList.d == null && !state.progressStatus.isInitial) {
                return const Expanded(
                  child: Center(
                    child: Text("No Movie Found"),
                  ),
                );
              }
              if (state.progressStatus.isFailure) {
                return Expanded(
                  child: Center(
                    child: Text(state.errMsg),
                  ),
                );
              }
              return MovieListView(movies: movieList);
            },
          ),
        ],
      ),
    );
  }
}
