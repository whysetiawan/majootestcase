class MovieResponseModel {
  final List<D>? d;
  final String? q;
  final int? v;

  const MovieResponseModel({this.d, this.q, this.v});

  @override
  String toString() => 'MovieResponseModel(d: $d, q: $q, v: $v)';

  factory MovieResponseModel.fromJson(Map<String, dynamic> json) {
    return MovieResponseModel(
      d: (json['d'] as List<dynamic>?)
          ?.map((e) => D.fromJson(e as Map<String, dynamic>))
          .toList(),
      q: json['q'] as String?,
      v: json['v'] as int?,
    );
  }

  Map<String, dynamic> toJson() => {
        'd': d?.map((e) => e.toJson()).toList(),
        'q': q,
        'v': v,
      };

  MovieResponseModel copyWith({
    List<D>? d,
    String? q,
    int? v,
  }) {
    return MovieResponseModel(
      d: d ?? this.d,
      q: q ?? this.q,
      v: v ?? this.v,
    );
  }
}

class V {
  final I? i;
  final String? id;
  final String? l;
  final String? s;

  const V({this.i, this.id, this.l, this.s});

  @override
  String toString() => 'V(i: $i, id: $id, l: $l, s: $s)';

  factory V.fromJson(Map<String, dynamic> json) => V(
        i: json['i'] == null
            ? null
            : I.fromJson(json['i'] as Map<String, dynamic>),
        id: json['id'] as String?,
        l: json['l'] as String?,
        s: json['s'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'i': i?.toJson(),
        'id': id,
        'l': l,
        's': s,
      };

  V copyWith({
    I? i,
    String? id,
    String? l,
    String? s,
  }) {
    return V(
      i: i ?? this.i,
      id: id ?? this.id,
      l: l ?? this.l,
      s: s ?? this.s,
    );
  }
}

class I {
  final int? height;
  final String? imageUrl;
  final int? width;

  const I({this.height, this.imageUrl, this.width});

  @override
  String toString() {
    return 'I(height: $height, imageUrl: $imageUrl, width: $width)';
  }

  factory I.fromJson(Map<String, dynamic> json) => I(
        height: json['height'] as int?,
        imageUrl: json['imageUrl'] as String?,
        width: json['width'] as int?,
      );

  Map<String, dynamic> toJson() => {
        'height': height,
        'imageUrl': imageUrl,
        'width': width,
      };

  I copyWith({
    int? height,
    String? imageUrl,
    int? width,
  }) {
    return I(
      height: height ?? this.height,
      imageUrl: imageUrl ?? this.imageUrl,
      width: width ?? this.width,
    );
  }
}

class D {
  final I? i;
  final String? id;
  final String? l;
  final String? q;
  final int? rank;
  final String? s;
  final List<V>? v;
  final int? vt;
  final int? y;
  final String? yr;

  const D({
    this.i,
    this.id,
    this.l,
    this.q,
    this.rank,
    this.s,
    this.v,
    this.vt,
    this.y,
    this.yr,
  });

  @override
  String toString() {
    return 'D(i: $i, id: $id, l: $l, q: $q, rank: $rank, s: $s, v: $v, vt: $vt, y: $y, yr: $yr)';
  }

  factory D.fromJson(Map<String, dynamic> json) => D(
        i: json['i'] == null
            ? null
            : I.fromJson(json['i'] as Map<String, dynamic>),
        id: json['id'] as String?,
        l: json['l'] as String?,
        q: json['q'] as String?,
        rank: json['rank'] as int?,
        s: json['s'] as String?,
        v: (json['v'] as List<dynamic>?)
            ?.map((e) => V.fromJson(e as Map<String, dynamic>))
            .toList(),
        vt: json['vt'] as int?,
        y: json['y'] as int?,
        yr: json['yr'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'i': i?.toJson(),
        'id': id,
        'l': l,
        'q': q,
        'rank': rank,
        's': s,
        'v': v?.map((e) => e.toJson()).toList(),
        'vt': vt,
        'y': y,
        'yr': yr,
      };

  D copyWith({
    I? i,
    String? id,
    String? l,
    String? q,
    int? rank,
    String? s,
    List<V>? v,
    int? vt,
    int? y,
    String? yr,
  }) {
    return D(
      i: i ?? this.i,
      id: id ?? this.id,
      l: l ?? this.l,
      q: q ?? this.q,
      rank: rank ?? this.rank,
      s: s ?? this.s,
      v: v ?? this.v,
      vt: vt ?? this.vt,
      y: y ?? this.y,
      yr: yr ?? this.yr,
    );
  }
}
