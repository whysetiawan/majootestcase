import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/features/home/movie/bloc/movie_bloc.dart';
import 'package:majootestcase/features/home/movie/widgets/movie_desc_text.dart';
import 'package:majootestcase/shared/constants/styles.dart';
import 'package:majootestcase/shared/widgets/page_template.dart';

const movieDetailPageRoute = '/movie-detail';

class MovieDetailPage extends StatelessWidget {
  const MovieDetailPage({Key? key}) : super(key: key);

  double calculateExpandedSize(MovieState state) {
    final deviceHeight = ScreenUtil.defaultSize.height;
    final imgHeight =
        double.parse((state.selectedMovie.i?.height ?? 0).toString());
    final imgWidth =
        double.parse((state.selectedMovie.i?.height ?? 0).toString());
    if (imgWidth > imgHeight || imgHeight > deviceHeight) {
      return deviceHeight * 0.8;
    }
    return imgHeight;
  }

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      title: "Movie List",
      extendBodyBehindAppBar: true,
      useSafeArea: false,
      withoutAppBar: true,
      scrollable: false,
      child: CustomScrollView(
        slivers: <Widget>[
          BlocBuilder<MovieBloc, MovieState>(
            builder: (context, state) {
              final selectedMovie = state.selectedMovie;
              return SliverAppBar(
                // pinned: true,
                // title: Text(selectedMovie.l ?? "Movie Detail"),
                systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
                  statusBarColor: Colors.transparent,
                ),
                // pinned: true,
                // floating: true,
                expandedHeight: calculateExpandedSize(state),
                flexibleSpace: FlexibleSpaceBar(
                  collapseMode: CollapseMode.parallax,
                  title: Text(
                    selectedMovie.l ?? "Movie Detail",
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  stretchModes: const <StretchMode>[
                    StretchMode.zoomBackground,
                    StretchMode.blurBackground,
                  ],
                  background: CachedNetworkImage(
                    imageUrl: selectedMovie.i?.imageUrl ?? "",
                    // height: 240.w,
                    fit: BoxFit.fitWidth,
                  ),
                ),
              );
            },
          ),
          SliverToBoxAdapter(
            child: BlocBuilder<MovieBloc, MovieState>(
              builder: (_, state) {
                final selectedMovie = state.selectedMovie;
                return Padding(
                  padding: EdgeInsets.all(Insets.lg),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      MovieDescText(
                        title: "Title : ",
                        text: selectedMovie.l ?? "",
                      ),
                      MovieDescText(
                        title: "Stars : ",
                        text: selectedMovie.s ?? "",
                      ),
                      MovieDescText(
                        title: "Year : ",
                        text: selectedMovie.yr ?? "",
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
          SliverToBoxAdapter(
            child: BlocBuilder<MovieBloc, MovieState>(
              builder: (context, state) {
                final selectedMovie = state.selectedMovie;
                final series = selectedMovie.v ?? [];
                return ListView.builder(
                  shrinkWrap: true,
                  itemCount: series.length,
                  itemBuilder: (context, index) {
                    return Card(
                      child: ListTile(
                        title: Text(series[index].l ?? ""),
                        subtitle: Text(series[index].s ?? ""),
                        leading: Container(
                          width: 60.w,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4.w),
                            image: DecorationImage(
                              image: CachedNetworkImageProvider(
                                series[index].i?.imageUrl ??
                                    ""
                                        "https://www.salonlfc.com/wp-content/uploads/2018/01/image-not-found-1-scaled-1150x647.png",
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
