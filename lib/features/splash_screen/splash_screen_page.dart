import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/features/home/home_navigator.dart';
import 'package:majootestcase/features/login/cubit/auth_cubit.dart';
import 'package:majootestcase/features/login/login_page.dart';
import 'package:majootestcase/shared/constants/app_icons.dart';

const splashScreenPageRoute = '/';

class SplashScreenPage extends StatefulWidget {
  const SplashScreenPage({Key? key}) : super(key: key);

  @override
  State<SplashScreenPage> createState() => _SplashScreenPageState();
}

class _SplashScreenPageState extends State<SplashScreenPage> {
  @override
  void initState() {
    () async {
      final _authCubit = BlocProvider.of<AuthCubit>(context);
      if (_authCubit.state.authStatus.isAuthenticated &&
          _authCubit.state.user.email != null) {
        return Future.delayed(const Duration(seconds: 3), () {
          Navigator.pushReplacementNamed(context, homePageRoute);
        });
      }
      return Future.delayed(const Duration(seconds: 3), () {
        Navigator.pushReplacementNamed(context, loginPageRoute);
      });
    }();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Image.asset(
              AppIcons.icLogo,
              width: 180.w,
              height: 180.w,
            ),
          ),
          Text(
            "Majoo Indonesia",
            style: Theme.of(context).textTheme.headline4,
          )
        ],
      ),
    );
  }
}
