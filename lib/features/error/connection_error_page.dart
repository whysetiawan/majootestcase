import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/features/home/cubit/connectivity_cubit.dart';
import 'package:majootestcase/shared/constants/app_colors.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';
import 'package:majootestcase/shared/constants/styles.dart';

import 'package:majootestcase/shared/widgets/page_template.dart';

const connectionErrorPageRoute = "/error-page";

class ConnectionErrorPage extends StatelessWidget {
  const ConnectionErrorPage({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ConnectivityCubit, ConnectivityState>(
      builder: (context, state) {
        return WillPopScope(
          onWillPop: () => Future.value(state.isConnected),
          child: PageTemplate(
            useSafeArea: false,
            scrollable: false,
            withoutAppBar: true,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Icon(
                    Icons.network_check_rounded,
                    size: 60,
                    color: Colors.red[700],
                  ),
                  const Text(
                    "No Internet Connection",
                  ),
                  Padding(
                    padding: EdgeInsets.all(Insets.xxl),
                    child: Column(
                      children: [
                        state.progressStatus.isLoading
                            ? const CircularProgressIndicator()
                            : IconButton(
                                onPressed:
                                    BlocProvider.of<ConnectivityCubit>(context)
                                        .connectivityCheck,
                                icon: const Icon(
                                  Icons.refresh,
                                  size: 32,
                                ),
                              ),
                        const Text(
                          "Retry",
                          style: TextStyle(
                            color: AppColors.secondary,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
