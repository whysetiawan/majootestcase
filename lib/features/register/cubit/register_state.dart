part of 'register_cubit.dart';

class RegisterState extends Equatable {
  final ProgressStatus registerProgressStatus;
  final bool isPasswordVisible;
  final bool isConfirmPasswordVisible;
  final String errMsg;

  const RegisterState({
    this.registerProgressStatus = ProgressStatus.initial,
    this.isPasswordVisible = false,
    this.isConfirmPasswordVisible = false,
    this.errMsg = '',
  });

  @override
  List<Object> get props => [
        registerProgressStatus,
        isPasswordVisible,
        isConfirmPasswordVisible,
        errMsg
      ];

  RegisterState copyWith({
    ProgressStatus? registerProgressStatus,
    bool? isPasswordVisible,
    bool? isConfirmPasswordVisible,
    String? errMsg,
  }) {
    return RegisterState(
      registerProgressStatus:
          registerProgressStatus ?? this.registerProgressStatus,
      isPasswordVisible: isPasswordVisible ?? this.isPasswordVisible,
      isConfirmPasswordVisible:
          isConfirmPasswordVisible ?? this.isConfirmPasswordVisible,
      errMsg: errMsg ?? this.errMsg,
    );
  }

  @override
  String toString() {
    return 'RegisterState(registerProgressStatus: $registerProgressStatus, isPasswordVisible: $isPasswordVisible, isConfirmPasswordVisible: $isConfirmPasswordVisible, errMsg: $errMsg)';
  }
}
