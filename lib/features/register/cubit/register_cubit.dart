import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';
import 'package:majootestcase/shared/helpers/db_service.dart';
import 'package:sqflite/sqlite_api.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit({
    required this.dbService,
  }) : super(const RegisterState());

  final DbService dbService;

  void toggleIsPasswordVisible() {
    emit(
      state.copyWith(isPasswordVisible: !state.isPasswordVisible),
    );
  }

  void toggleIsConfirmPasswordVisible() {
    emit(
      state.copyWith(isConfirmPasswordVisible: !state.isConfirmPasswordVisible),
    );
  }

  Future<void> register({
    required String email,
    required String password,
  }) async {
    try {
      emit(
        state.copyWith(registerProgressStatus: ProgressStatus.loading),
      );
      final res = await dbService.insert(
        email: email,
        password: password,
      );
      if (res > 0) {
        emit(state.copyWith(registerProgressStatus: ProgressStatus.success));
      } else {
        throw 'Failed to register';
      }
    } on DatabaseException catch (e) {
      late String errorMessage;
      if (e.isUniqueConstraintError()) {
        errorMessage = 'Email already registered';
      } else {
        errorMessage = 'Failed to register';
      }
      emit(
        state.copyWith(
          registerProgressStatus: ProgressStatus.failure,
          errMsg: errorMessage,
        ),
      );
    } catch (e) {
      emit(
        state.copyWith(
          registerProgressStatus: ProgressStatus.failure,
          errMsg: e.toString(),
        ),
      );
    }
  }
}
