import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:majootestcase/features/register/cubit/register_cubit.dart';
import 'package:majootestcase/shared/constants/app_icons.dart';
import 'package:majootestcase/shared/constants/progress_status.dart';
import 'package:majootestcase/shared/constants/styles.dart';
import 'package:majootestcase/shared/helpers/form_validator.dart';
import 'package:majootestcase/shared/widgets/app_button.dart';
import 'package:majootestcase/shared/widgets/app_snack_bar.dart';
import 'package:majootestcase/shared/widgets/page_template.dart';
import 'package:majootestcase/shared/widgets/space.dart';
import 'package:majootestcase/shared/widgets/top_label_input.dart';

const registerPageRoute = '/register';

class RegisterPage extends StatefulWidget {
  const RegisterPage({
    Key? key,
  }) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();

  final _emailController = TextEditingController();

  final _passwordController = TextEditingController();

  final _confirmPasswordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return PageTemplate(
      overlayStyle: SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.transparent,
      ),
      appBar: AppBar(
        title: const Text('Register'),
        backgroundColor: Colors.white,
        titleTextStyle: const TextStyle(color: Colors.black),
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: Insets.xl,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center(
              child: Image.asset(
                AppIcons.icLogo,
                width: 180.w,
                height: 180.w,
              ),
            ),
            VerticalSpace(height: 16.w),
            Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  TopLabelInput(
                    labelText: "Email",
                    controller: _emailController,
                    validator: (value) {
                      if (value?.isEmpty == true) {
                        return "Email is required";
                      }
                      if (FormValidator.isEmail(value ?? '')) {
                        return "Email is invalid";
                      }
                      return null;
                    },
                  ),
                  VerticalSpace(height: 16.w),
                  BlocBuilder<RegisterCubit, RegisterState>(
                    builder: (context, state) {
                      return TopLabelInput(
                        labelText: "Password",
                        controller: _passwordController,
                        validator: (value) {
                          if (value?.isEmpty == true) {
                            return "Password is required";
                          }
                          if (value!.length < 8) {
                            return "Minimum password length is 8";
                          }
                          return null;
                        },
                        obscureText: !state.isPasswordVisible,
                        decoration: InputDecoration(
                          suffixIcon: InkWell(
                            onTap: BlocProvider.of<RegisterCubit>(context)
                                .toggleIsPasswordVisible,
                            child: Icon(
                              !state.isPasswordVisible
                                  ? Icons.visibility_off_rounded
                                  : Icons.visibility_rounded,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                  VerticalSpace(height: 16.w),
                  BlocBuilder<RegisterCubit, RegisterState>(
                    builder: (context, state) {
                      return TopLabelInput(
                        labelText: "Confirm Password",
                        controller: _confirmPasswordController,
                        obscureText: !state.isConfirmPasswordVisible,
                        validator: (value) {
                          if (value?.isEmpty == true) {
                            return "Confirm Password is required";
                          }
                          if (value != _passwordController.text) {
                            return "Password doesn't match";
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          suffixIcon: InkWell(
                            onTap: BlocProvider.of<RegisterCubit>(context)
                                .toggleIsConfirmPasswordVisible,
                            child: Icon(
                              !state.isConfirmPasswordVisible
                                  ? Icons.visibility_off_rounded
                                  : Icons.visibility_rounded,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
            VerticalSpace(height: 16.w),
            BlocConsumer<RegisterCubit, RegisterState>(
              listener: (_, state) {
                if (state.registerProgressStatus.isSuccess) {
                  ScaffoldMessenger.of(context)
                    ..hideCurrentSnackBar
                    ..showSnackBar(
                      AppSnackBar.successSnackBar(
                        message: "Registration Success, Please Login",
                      ),
                    );
                  Navigator.of(context).pop();
                } else if (state.registerProgressStatus.isFailure) {
                  ScaffoldMessenger.of(context)
                    ..hideCurrentSnackBar
                    ..showSnackBar(
                      AppSnackBar.errorSnackBar(
                        message: state.errMsg,
                      ),
                    );
                }
              },
              builder: (context, state) {
                return AppButton(
                  title: "Register",
                  onPressed: state.registerProgressStatus.isLoading
                      ? null
                      : () {
                          if (_formKey.currentState?.validate() == true) {
                            BlocProvider.of<RegisterCubit>(context).register(
                              email: _emailController.text,
                              password: _passwordController.text,
                              // confirmPassword: _confirmPasswordController.text,
                            );
                          }
                        },
                );
              },
            ),
            VerticalSpace(height: 16.w),
          ],
        ),
      ),
    );
  }
}
