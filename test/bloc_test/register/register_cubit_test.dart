// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/features/register/cubit/register_cubit.dart';
import 'package:majootestcase/shared/helpers/db_service.dart';

class MockRegisterCubit extends MockCubit<RegisterState>
    implements RegisterCubit {}

void main() async {
  group('Register Cubit', () {
    late RegisterCubit registerCubit;
    late DbService dbService;

    setUp(() {
      dbService = DbService();
      registerCubit = RegisterCubit(dbService: dbService);
    });

    blocTest<RegisterCubit, RegisterState>(
      "Toggle Confirm Password Cubit",
      build: () => registerCubit,
      act: (bloc) => bloc
        ..toggleIsConfirmPasswordVisible()
        ..toggleIsConfirmPasswordVisible(),
      expect: () => [
        const RegisterState(
          isConfirmPasswordVisible: true,
        ),
        const RegisterState(
          isConfirmPasswordVisible: false,
        ),
      ],
    );

    blocTest<RegisterCubit, RegisterState>(
      "Toggle Password Visiblity",
      build: () => registerCubit,
      act: (bloc) => bloc
        ..toggleIsPasswordVisible()
        ..toggleIsPasswordVisible(),
      expect: () => [
        const RegisterState(
          isPasswordVisible: true,
        ),
        const RegisterState(
          isPasswordVisible: false,
        ),
      ],
    );
  });
}
