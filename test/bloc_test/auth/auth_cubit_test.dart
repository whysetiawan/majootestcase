// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:bloc_test/bloc_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:majootestcase/features/login/cubit/auth_cubit.dart';
import 'package:majootestcase/shared/helpers/db_service.dart';

import '../helpers/hydrated_storage.dart';

class MockAuthCubit extends MockCubit<AuthState> implements AuthCubit {}

void main() async {
  group('Auth Cubit', () {
    late AuthCubit authCubit;
    late DbService dbService;
    setUp(() {
      dbService = DbService();
      authCubit = mockHydratedStorage(
        () => AuthCubit(
          dbService: dbService,
        ),
      );
    });
    test("Initialize Auth Cubit", () {
      expect(authCubit.state, const AuthState());
    });

    blocTest<AuthCubit, AuthState>(
      "Toggle Visiblity Password",
      build: () {
        return authCubit;
      },
      act: (bloc) => bloc
        ..toggleVisiblity()
        ..toggleVisiblity(),
      expect: () => [
        const AuthState(isPasswordVisible: true),
        const AuthState(isPasswordVisible: false),
      ],
    );

    blocTest<AuthCubit, AuthState>(
      "Change Auth Status",
      build: () {
        return authCubit;
      },
      act: (bloc) => bloc
        ..authStatusChanged(AuthStatus.authenticated)
        ..authStatusChanged(AuthStatus.unauthenticated),
      expect: () => [
        const AuthState(authStatus: AuthStatus.authenticated),
        const AuthState(authStatus: AuthStatus.unauthenticated),
      ],
    );

    // blocTest<AuthCubit, AuthState>(
    //   "Login",
    //   build: () {
    //     return authCubit;
    //   },
    //   act: (bloc) async {
    //     await bloc.login("whysetiawan27@gmail.coma", "test12345");
    //   },
    //   verify: (bloc) {
    //     verify(() => DbService().findUser(any(), any())).called(1);
    //   },
    //   expect: () => [
    //     const AuthState(
    //       loginProgressStatus: ProgressStatus.loading,
    //       errMsg: '',
    //     ),
    //     const AuthState(
    //       loginProgressStatus: ProgressStatus.failure,
    //       errMsg: "Invalid Username or Password",
    //     ),
    //   ],
    // );

    tearDown(() {
      authCubit.close();
    });
  });
}
