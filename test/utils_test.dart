// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter_test/flutter_test.dart';

import 'package:majootestcase/shared/helpers/db_service.dart';

void main() {
  group("Check Db Service singleton", () {
    test("Check Db Service singleton", () async {
      var service1 = DbService();
      var service2 = DbService();

      expect(service1, service2);
    });
  });
}
